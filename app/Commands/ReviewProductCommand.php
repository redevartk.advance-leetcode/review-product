<?php

namespace App\Commands;

use App\Services\ProductService;
use LaravelZero\Framework\Commands\Command;

class ReviewProductCommand extends Command
{

    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'review:product {productId : Id for product}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'List review of product';

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle(ProductService $productService)
    {
        // Access the productId parameter using $this->argument('productId')
        $productId = $this->argument('productId');

        // Now you can use $productId in your logic
        $this->info($productService->reviewProduct([
            "productId" => $productId
        ]));
    }
}
