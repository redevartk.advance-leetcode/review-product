<?php

namespace App\Services;

use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
// nope, not work with laravel zero, only with normal laravel
// use Illuminate\Support\Facades\Validator;

class ProductService
{
    private $productJsonFile;
    private $reviewJsonFile;

    //config
    private $config;

    public function __construct(){
        $this->productJsonFile = base_path("database\\products.json");
        $this->reviewJsonFile = base_path("database\\reviews.json");

        //this is for sake of this project only
        //no specific requirement given on how cache for this should be
        //do not do this if it was PRODUCTION / WORKING BUSINESS PROJECT
        $this->config = [
            'userId' => 123, // set static for now, it should be in .env if possible
            'enableGlobalSummaryCaching' => true,
            'cachingTime' => 60, //in minutes
        ];
        
    }

    private function reviewArrayBuilder($data = []){
        //supposed it will be better if we have validator in laravel zero
        // $data = Validator::make($data, [
        //     'getStarArrOnly' => 'nullable|boolean',
        // ])->validate();

        //manual validation
        if (isset($data['getStarArrOnly']) && !is_bool($data['getStarArrOnly'])) {
            throw new \Exception('Invalid value for getStarArrOnly. Must be a boolean.');
        }

        $arrBuilder = [
            "total_reviews" => 0,
            "average_ratings" => 0,
            "5_star" => 0,
            "4_star" => 0,
            "3_star" => 0,
            "2_star" => 0,
            "1_star" => 0
        ];

        if(!empty($data) && $data['getStarArrOnly'] === true){
            unset($arrBuilder['$total_reviews']);
            unset($arrBuilder['$average_ratings']);
        }

        return $arrBuilder;
    }

    private function calculateAverageRating($data){
        //supposed it will be better if we have validator in laravel zero
        // $data = Validator::make($data, [
        //     '1_star' => 'required|integer',
        //     '2_star' => 'required|integer',
        //     '3_star' => 'required|integer',
        //     '4_star' => 'required|integer',
        //     '5_star' => 'required|integer',
        // ])->validate();

        //manual validation
        foreach (['1_star', '2_star', '3_star', '4_star', '5_star'] as $key) {
            if (!isset($data[$key]) || !is_numeric($data[$key])) {
                Log::error("Invalid or missing value for $key.");
                throw new \Exception('Invalid or missing value for $key.');
            }
        }

        // formula to create a rating from 0.0 to 5.0
        $weightedSum = (1 * $data['1_star']) + (2 * $data['2_star']) + (3 * $data['3_star']) + (4 * $data['4_star']) + (5 * $data['5_star']);
        $totalReviews = $data['1_star'] + $data['2_star'] + $data['3_star'] + $data['4_star'] + $data['5_star'];
        $averageRating = $totalReviews > 0 ? $weightedSum / $totalReviews : 0;

        //format to one decimal, convert to float/double
        $averageRatingResult = (float)number_format($averageRating, 1);

        //result
        return $averageRatingResult;
    }

    private function getTotalPerRating($data){
        //supposed it will be better if we have validator in laravel zero
        // $data = Validator::make($data, [
        //     'reviewData' => 'required|json',
        // ])->validate();
        
        //manual validation
        $reviewData = $data['reviewData'];

        if ($reviewData === null && json_last_error() !== JSON_ERROR_NONE) {
            Log::error('Invalid JSON format for reviewData.');
            throw new \Exception('Invalid JSON format for reviewData.');
        }

        //create rating array
        $ratingArrResult = $this->reviewArrayBuilder([
            "getStarArrOnly" => true
        ]);

        //filter every star number
        foreach($reviewData as $data){

            //rating calc
            switch ($data['rating']) {
                case 1:
                    $ratingArrResult['1_star'] += 1;
                    break;
                case 2:
                    $ratingArrResult['2_star'] += 1;
                    break;
                case 3:
                    $ratingArrResult['3_star'] += 1;
                    break;
                case 4:
                    $ratingArrResult['4_star'] += 1;
                    break;
                case 5:
                    $ratingArrResult['5_star'] += 1;
                    break;
                default:
                    //do nothing
                    break;
            }
        }

        //result
        return $ratingArrResult;
    }

    private function filterSpecificProduct($data){

        //validate input
        if(empty($data['reviewData'])){
            throw new \Exception('Invalid reviewData or no reviewData inserted.');
        }
    
        if(empty($data['productId'])){
            throw new \Exception('Invalid productId or no productId inserted.');
        }
    
        //get some data by specific productId only
        $filteredSpecificProductData = [];
        foreach($data['reviewData'] as $reviewData){
            if ($reviewData['product_id'] === intval($data['productId'])) {
                $filteredSpecificProductData[] = $reviewData;
            }
        }
    
        //result
        return $filteredSpecificProductData;
    }

    public function reviewSummary(){
        try {
            //return cached result
            if($this->config['enableGlobalSummaryCaching'] === true){
                if(!empty(Cache::get('userData')) && !empty(Cache::get('userData')['reviewSummaryResult'])){
                    return Cache::get('userData')['reviewSummaryResult'];
                }
            }

            $reviewJsonContent = file_get_contents($this->reviewJsonFile);
            $reviewData = json_decode($reviewJsonContent, true);

            //init empty result
            $reviewResult =  $this->reviewArrayBuilder();

            //init per-variable data
            $totalPerRating = $this->getTotalPerRating([
                "reviewData" => $reviewData
            ]);
            $averageRating = $this->calculateAverageRating($totalPerRating);
            $totalReview = count($reviewData);

            //build array
            $reviewResult = array_merge($reviewResult, $totalPerRating);
            $reviewResult['total_reviews'] = $totalReview;
            $reviewResult['average_ratings'] = $averageRating;

            //result
            $result = json_encode($reviewResult);

            if($this->config['enableGlobalSummaryCaching'] === true){
                Cache::put('userData', ['userId' => $this->config['userId'], 'reviewSummaryResult' => $result], $this->config['cachingTime']);
            }

            return $result;
        } catch (\Throwable $th) {
            Log::error($th->getMessage()." on line: ".$th->getLine());
            return json_encode([
                "data" => "data is empty ?", "messages" => $th->getMessage(), "on line: " => $th->getLine()
            ]);
        }
    }

    public function reviewProduct($data){
        try {
            if(empty($data['productId'])){
                throw new \Exception('Invalid productId or no productId inserted.');
            }

            $reviewJsonContent = file_get_contents($this->reviewJsonFile);
            $reviewData = json_decode($reviewJsonContent, true);

            //filter specificProduct
            $reviewData = $this->filterSpecificProduct([
                "reviewData" => $reviewData,
                "productId" => $data['productId']
            ]);

            //init empty result
            $reviewResult =  $this->reviewArrayBuilder();

            //init per-variable data
            $totalPerRating = $this->getTotalPerRating([
                "reviewData" => $reviewData
            ]);
            $averageRating = $this->calculateAverageRating($totalPerRating);
            $totalReview = count($reviewData);

            //build array
            $reviewResult = array_merge($reviewResult, $totalPerRating);
            $reviewResult['total_reviews'] = $totalReview;
            $reviewResult['average_ratings'] = $averageRating;

            $result = json_encode($reviewResult);
            return $result;

        } catch (\Throwable $th) {
            Log::error($th->getMessage()." on line: ".$th->getLine());
            return json_encode([
                "data" => "data is empty ?", "messages" => $th->getMessage(), "on line: " => $th->getLine()
            ]);
        }
    }
}
