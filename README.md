# [Advance Leetcode] Review Products

This app is designed for reviewing products and generating product summary based on a JSON file.

## Properties

* Programming Lang: PHP 8.2.3
* Framework: Laravel Zero 8.8^
* Database: MySQL 5.7.33^

## Installation

1. Clone Repository
2. Run "composer install"

## Testings

* Basically there's no API for this, it's just direct console command
* You can run "php application review:product 1" or 2 or 3, you can check id on database/products.json
* You can run "php artisan review:summary" to get whole product summary
